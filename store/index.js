import Vue from 'vue'
import Vuex from 'vuex'
import Api from '@/common/vmeitime-http/'

const util = require('@/common/util.js');
const rootUrl = 'https://anranweb.cn'
Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,	//是否已登录
		loginProvider: "",
		openid: null,
		listData: [],
		userInfo:{}, //用户信息
		authSetting:{},// 授权列表
		addressMapBg:'',	// 分享定位背景图
		erweimaWeChat: '', // 二维码
		isRefresh: false,	// 是否刷新数据
	},
	mutations: {
		login(state, provider) {
			state.hasLogin = true;
			state.loginProvider = provider;
		},
		logout(state) {
			state.hasLogin = false
			state.openid = null
		},
		setOpenid(state, openid) {
			state.openid = openid
		},
		setListData(state, listData){
			state.listData = listData
		},
		setUserInfo(state, userInfo) {
			state.userInfo = userInfo
		},
		setAuthSetting(state, obj){
			state.authSetting = obj
		},
		setAddressMapBg(state, obj){
			state.addressMapBg = obj
		},
		setErweimaWeChat(state, obj){
			state.erweimaWeChat = obj
		},
		setIsRefresh(state, bool){
			state.isRefresh = bool
		}
	},
	actions: {
		// lazy loading openid
		getUserOpenId: async function ({commit,state}) {
			return await new Promise((resolve, reject) => {
				if (state.openid) {
					resolve(state.openid)
				} else {
					uni.login({
						success: (data) => {
							commit('login')
							let params = {
							  code: data.code
							}
							uni.request({
								url: rootUrl + '/php-phalapi/public/?service=App.Wx_WeiXin.WxGetOpenid',
								data: params,
								method: 'POST',
								header: {
									'content-type': 'application/x-www-form-urlencoded'
								},
								success: (res) => {
									let openid = res.data.data.openid
									commit('setOpenid', openid)
									resolve(openid)
								}
							});
							
						},
						fail: (err) => {
							console.log('uni.login 接口调用失败，将无法正常使用开放接口等服务', err)
							reject(err)
						}
					})
				}
			})
		},
		
		// 获取授权状态
		getSetting({commit,state}){	
			return new Promise((resolve, reject) =>{
				if(util.isEmptyValue(state.authSetting)){
					uni.getSetting({
					   success(res) {
						  /* let nd = {
							  scopeUserLocation: res.authSetting[scope.userLocation]?true:false,
							  scopeUserInfo:res.authSetting[scope.userInfo]?true:false
						  } */
						  commit('setAuthSetting', res.authSetting)
						  resolve(res.authSetting)
					   }
					})
				}else{
					resolve(state.authSetting)
				}
			})
		},
		
		// 获取用户信息
		getUserInfo({commit,state}) {
			return new Promise((resolve, reject) => {
				if (util.isEmptyValue(state.userInfo)) {
					// 获取用户信息
					uni.getUserInfo({
					  provider: 'weixin',
					  success: function (infoRes) {
						commit('setUserInfo', infoRes.userInfo)
						resolve(infoRes.userInfo)
					  }
					});
				} else {
					resolve(state.userInfo)
				}
			})
		},
		
		getAddressMapBg({commit,state}){	// 分享 定位图背景
			return new Promise((resolve, reject) => {
				if (state.addressMapBg) {
					resolve(state.addressMapBg)
				} else {
					uni.getImageInfo({
					  src: rootUrl +'/uploadimg/share_bg.png', //share_bg.png map_address.png
					  success(res) {
						commit('setAddressMapBg', res.path)
						resolve(res.path)
					  }
					})
				}
				
			})
		},	
		
		getErweimaWeChat({commit,state}){	// 二维码图片
			return new Promise((resolve, reject) => {
				if (state.erweimaWeChat) {
					resolve(state.erweimaWeChat)
				} else {
					uni.getImageInfo({
					  src: rootUrl + '/uploadimg/anranWeChat.jpg',
					  success(res) {
						commit('setErweimaWeChat', res.path)
						resolve(res.path)
					  }
					})
				}
				
			})
		},	
		
		
	}
})

export default store
