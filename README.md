# wx-uni-app

#### 介绍
安然打卡小程序，使用uni-app框架开发。

#### 预览地址
![](http://www.anranweb.cn/uploadimg/anranWeChat.jpg)

微信搜索小程序 ‘安然打卡’。

#### 软件架构
~~~
┌─common            	第三方JS或公用css  
│  └─vmeitime-http      所有的接口
├─components            uni-app组件目录
│  └─comp-a.vue         可复用的a组件
├─pages                 业务页面文件存放的目录
│  ├─index
│  │  └─index.vue       index页面
│  └─list
│     └─list.vue        list页面
├─static                存放应用引用静态资源（如图片、视频等）的目录，注意：静态资源只能存放于此
├─main.js               Vue初始化入口文件
├─App.vue               应用配置，用来配置App全局样式以及监听 应用生命周期
├─manifest.json         配置应用名称、appid、logo、版本等打包信息，详见
└─pages.json            配置页面路由、导航条、选项卡等页面类信息，详见
~~~


#### 使用说明
~~~
开发
使用HBuilder开发工具打开项目，‘运行’->‘运行到小程序模拟器’->‘微信开发者工具’。

发布
‘发行’->‘小程序-微信’
~~~

>其他参考uni-app框架[官网](https://uniapp.dcloud.io/),