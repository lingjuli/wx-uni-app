import http from './interface'

/**
 * 将业务所有接口统一起来便于维护
 * 如果项目很大可以将 url 独立成文件，接口分成不同的模块
 * 
 */

// 登录
export const loginSing = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.LoginSign',
        data,
    })
}

// 打卡
export const addSing = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.AddSign',
        data,
    })
}

// 查询当天是否打卡
export const selectDataTime = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.WxSelectData',
        data,
    })
}

// 查询打卡记录
export const getSignList = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.GetUserSign',
        data,
    })
}

// 获取小程序码
export const getCode = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.GetCode',
        data,
    })
}

// 查询所有用户打卡记录
export const selectAllUsersign = (data) =>{
	return http.request({
		url: '?service=App.Wx_WeiXin.SelectAllUsersign',
		data,
	})
}

// 新增留言
export const addMsg = (data) => {
    return http.request({
        url: '?service=App.Web_Msg.AddMsg',
        data,
    })
}

// 查询系统配置
export const getSys = (data) =>{
	return http.request({
		url: '?service=App.Sys_Sys.SearchH5Sys',
		data,
	})
}

// 查询留言
export const getMsg = (data) =>{
	return http.request({
	    url: '?service=App.Web_Msg.SelectMsg',
	    data,
	})
}

// 用户打卡记录查询
export const wxGetUserSign = (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.GetUserSign',
        data,
    })
}

// 查询单条打卡详情
export const wxGetSignInfo= (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.SelectSign',
        data,
    })
}

// 查询用户信息
export const wxGetUserInfo= (data) => {
    return http.request({
        url: '?service=App.Wx_WeiXin.GetUserInfo',
        data,
    })
}

// 单独导出(测试接口) import {test} from '@/common/vmeitime-http/'
export const text = (data) => {
	http.config.baseUrl = "http://www.ceshi.cn:8071/"
	//设置请求前拦截器
	/* http.interceptor.request = (config) => {
		config.header = {
			"token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		}
	} */
	//设置请求结束后拦截器
	http.interceptor.response = (response) => {
		console.log('个性化response....')
		//判断返回状态 执行相应操作
		return response;
	}
    return http.request({
        url: '?service=App.Sys_User.Login',
		dataType: 'text',
        data,
    })
}



// 默认全部导出  import api from '@/common/vmeitime-http/'
export default {
	text,
	loginSing,
	addSing,
	selectDataTime,
	getSignList,
	getCode,
	selectAllUsersign,
	addMsg,
	getSys,
	getMsg,
	wxGetUserSign,
	wxGetSignInfo,
	wxGetUserInfo
}